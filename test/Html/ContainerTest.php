<?php

namespace youconix\Core\Test\HTML;

use youconix\Core\Html\Container;
use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Html;

class ContainerTest extends TestCase
{
    /** @var Html */
    private $factory;

    /** @var Container */
    private $container;

    public function setUp(): void
    {
        $this->factory = new Html();
        $this->container = $this->factory->div();
    }


    /**
     * @test
     */
    public function emptyContainer(): void
    {
        $expected = '<div ></div>';
        $this->assertEquals($expected, $this->container->generateItem());
    }

    /**
     * @test
     */
    public function divWithStringContent(): void
    {
        $content = 'normal string content';
        $this->container->setContent($content);

        $expected = '<div >'.$content.'</div>';
        $this->assertEquals($expected, $this->container->generateItem());
    }

    /**
     * @test
     */
    public function divWithHtmlContent(): void
    {
        $content = $this->factory->datetime('calender', '01-01-2000');
        $this->container->setContent($content);

        $expected = '<div ><input type="datetime" name="calender" value="01-01-2000" >'.PHP_EOL.'</div>';
        $this->assertEquals($expected, $this->container->generateItem());
    }

    /**
     * @test
     */
    public function divWithAddedStringContent(): void
    {
        $content = 'normal string content';
        $addedContent = 'added string content';
        $this->container->setContent($content);
        $this->container->addContent($addedContent);

        $expected = '<div >'.$content.$addedContent.'</div>';
        $this->assertEquals($expected, $this->container->generateItem());
    }
}
