<?php

namespace youconix\Core\Test\Html\Header;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Header\StylesheetLink;
use youconix\Core\Html\Html;

class StylesheetLinkTest extends TestCase
{
    /** @var Html */
    private $factory;

    /** @var StylesheetLink */
    private $link;

    /** @var string */
    private $url = 'http://example.com/test.css';

    public function setUp(): void
    {
        $this->factory = new Html();
        $this->link = $this->factory->stylesheetLink($this->url);
    }

    /**
     * @test
     */
    public function stylesheetLink(): void
    {
        $expected = '<link rel="stylesheet" href="' . $this->url . '" media="screen" >';

        $this->assertEquals($expected, $this->link->generateItem());
    }

    /**
     * @test
     */
    public function stylesheetLinkWithType(): void
    {
        $this->link->setType('text/css');
        $expected = '<link rel="stylesheet" href="' . $this->url . '" type="text/css" media="screen" >';

        $this->assertEquals($expected, $this->link->generateItem());
    }

    /**
     * @test
     */
    public function stylesheetLinkWithMedia(): void
    {
        $this->link->setMedia('print');
        $expected = '<link rel="stylesheet" href="' . $this->url . '" media="print" >';

        $this->assertEquals($expected, $this->link->generateItem());
    }
}