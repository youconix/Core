<?php

namespace youconix\Core\Test\Html\Header;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Header\Metatag;
use youconix\Core\Html\Html;

class MetatagTest extends TestCase
{
    /** @var Html */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new Html();
    }

    /**
     * @test
     */
    public function namedMetaTag(): void
    {
        $name = 'my-metatag';
        $content = 'test content';
        $metaTag = $this->factory->metatag($name, $content);

        $expected = '<meta name="' . $name . '" content="' . $content . '" >' . PHP_EOL;

        $this->assertEquals($expected, $metaTag->generateItem());
    }

    /**
     * @test
     * @dataProvider equiveProvider
     * @param string $name
     * @param string $content
     * @param string $expected
     */
    public function httpEquivMetaTag($name, $content, $expected): void
    {
        $metaTag = $this->factory->metatag($name, $content);

        $this->assertEquals($expected, $metaTag->generateItem());
    }

    /**
     * @test
     */
    public function metaTagWithScheme(): void
    {
        $name = 'my-metatag';
        $content = 'test content';
        $scheme = 'test-scheme';
        $metaTag = $this->factory->metatag($name, $content)->setScheme($scheme);

        $expected = '<meta scheme="' . $scheme . '" name="' . $name . '" content="' . $content . '" >' . PHP_EOL;

        $this->assertEquals($expected, $metaTag->generateItem());
    }

    /**
     * @return array
     */
    public function equiveProvider(): array
    {
        $names = ['refresh', 'charset', 'expires'];
        $content = 'test-content';
        $items = [];

        foreach ($names as $name) {
            $items[] = [
                $name,
                $content,
                '<meta http-equiv="' . $name . '" content="' . $content . '" >' . PHP_EOL
            ];
        }

        return $items;
    }
}