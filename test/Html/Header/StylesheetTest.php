<?php

namespace youconix\Core\Test\Html\Header;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Header\Stylesheet;
use youconix\Core\Html\Html;

class StylesheetTest extends TestCase
{
    /** @var Html */
    private $factory;

    /** @var Stylesheet */
    private $styleSheet;

    /** @var string */
    private $css;

    public function setUp(): void
    {
        $this->factory = new Html();
        $this->css = 'body { backgrond-color:F111; }';
        $this->styleSheet = $this->factory->stylesheet($this->css);
    }

    /**
     * @test
     */
    public function stylesheet(): void
    {
        $expected = '<style >' . PHP_EOL . '<!--' . PHP_EOL . $this->css . PHP_EOL . '//-->' . PHP_EOL . '</style>';

        $this->assertEquals($expected, $this->styleSheet->generateItem());
    }

    /**
     * @test
     */
    public function stylesheetWithType(): void
    {
        $this->styleSheet->setType('text/css');
        $expected = '<style type="text/css">' . PHP_EOL . '<!--' . PHP_EOL . $this->css . PHP_EOL . '//-->' . PHP_EOL . '</style>';

        $this->assertEquals($expected, $this->styleSheet->generateItem());
    }
}