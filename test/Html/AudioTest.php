<?php

namespace youconix\Core\Test\HTML;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Audio;
use youconix\Core\Html\Html;

class AudioTest extends TestCase
{
    /**
     * @var Audio
     */
    private $audio;

    /**
     * @var string
     */
    private $url = 'http://example.com/music.ogg';

    /**
     * @var string
     */
    private $type = 'ogg';

    public function setUp(): void
    {
        $factory = new Html();
        $this->audio = $factory->audio($this->url, $this->type);
    }

    /**
     * @test
     */
    public function addSource(): void
    {
        $sources = [
            ['http://example.com/music.mp3', 'mp3'],
            ['http://example.com/music.acc', 'acc']
        ];

        foreach ($sources as $source) {
            $this->audio->addSource($source[0], $source[1]);
        }

        $expected = '<audio >' . PHP_EOL .
            $this->getDefaultSource();
        foreach ($sources as $source) {
            $expected .= '<source src="' . $source[0] . '" type="audio/' . $source[1] . '">' . PHP_EOL;
        }
        $expected .= '</audio>' . PHP_EOL;

        $this->assertEquals($expected, $this->audio->generateItem());
    }

    /**
     * @test
     * @param string $loader
     * @param string $expected
     * @dataProvider loaderDataProvider
     */
    public function setLoader(string $loader, string $expected): void
    {
        $this->audio->setLoader($loader);

        $this->assertEquals($expected, $this->audio->generateItem());
    }

    /**
     * @test
     */
    public function loop(): void
    {
        $this->audio->loop(true);
        $expected = '<audio loop="loop" >' . PHP_EOL .
            $this->getDefaultSource() .
            '</audio>' . PHP_EOL;

        $this->assertEquals($expected, $this->audio->generateItem());
    }

    /**
     * @test
     */
    public function controls(): void
    {
        $this->audio->controls(true);
        $expected = '<audio controls="controls" >' . PHP_EOL .
            $this->getDefaultSource() .
            '</audio>' . PHP_EOL;

        $this->assertEquals($expected, $this->audio->generateItem());
    }

    /**
     * @test
     */
    public function autoplay(): void
    {
        $this->audio->autoplay(true);
        $expected = '<audio autoplay="autoplay" >' . PHP_EOL .
            $this->getDefaultSource() .
            '</audio>' . PHP_EOL;

        $this->assertEquals($expected, $this->audio->generateItem());
    }

    /**
     * @return array
     */
    public function loaderDataProvider(): array
    {
        $loaders = [
            'auto',
            'metadata',
            'none',
        ];

        $provider = [];
        foreach ($loaders as $loader) {
            $provider[] = [
                $loader,
                '<audio preload="' . $loader . '" >' . PHP_EOL .
                $this->getDefaultSource() .
                '</audio>' . PHP_EOL,
            ];
        }

        return $provider;
    }

    /**
     * @return string
     */
    private function getDefaultSource(): string
    {
        return '<source src="' . $this->url . '" type="audio/' . $this->type . '">' . PHP_EOL;
    }
}
