<?php

namespace youconix\Core\Test\HTML;

use youconix\Core\Html\Html;
use youconix\Core\Html\Video;
use PHPUnit\Framework\TestCase;

class VideoTest extends TestCase
{
    /**
     * @var Video
     */
    private $video;

    /**
     * @var string
     */
    private $url = 'http://example.com/video.WebM';

    /**
     * @var string
     */
    private $type = 'WebM';

    public function setUp(): void
    {
        $factory = new Html();
        $this->video = $factory->video($this->url, $this->type);
    }

    /**
     * @test
     */
    public function addSource(): void
    {
        $sources = [
            ['http://example.com/video.mp4', 'mp4'],
            ['http://example.com/music.avi', 'avi']
        ];

        foreach ($sources as $source) {
            $this->video->addSource($source[0], $source[1]);
        }

        $expected = '<video >' . PHP_EOL .
            $this->getDefaultSource();
        foreach ($sources as $source) {
            $expected .= '<source src="' . $source[0] . '" type="video/' . $source[1] . '">' . PHP_EOL;
        }
        $expected .= '</video>' . PHP_EOL;

        $this->assertEquals($expected, $this->video->generateItem());
    }

    /**
     * @test
     */
    public function setPreLoader(): void
    {
        $preLoader = '/icon.png';
        $this->video->setPreLoader($preLoader);

        $expected = '<video poster="' . $preLoader . '" >' . PHP_EOL .
            $this->getDefaultSource() .
            '</video>' . PHP_EOL;

        $this->assertEquals($expected, $this->video->generateItem());
    }

    /**
     * @return string
     */
    private function getDefaultSource(): ?string
    {
        return '<source src="' . $this->url . '" type="video/' . $this->type . '">' . PHP_EOL;;
    }
}
