<?php

namespace youconix\Core\Test\Html;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Container;
use youconix\Core\Html\Html;

class HeaderTest extends TestCase
{
    /** @var Html */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new Html();
    }

    /**
     * @test
     */
    public function H1(): void
    {
        $header = $this->factory->H1();
        $expected = '<h1 ></h1>';

        $this->assertInstanceOf(Container::class, $header);
        $this->assertEquals($expected, $header->generateItem());
    }

    /**
     * @test
     */
    public function H2(): void
    {
        $header = $this->factory->H2();
        $expected = '<h2 ></h2>';

        $this->assertInstanceOf(Container::class, $header);
        $this->assertEquals($expected, $header->generateItem());
    }

    /**
     * @test
     */
    public function H3(): void
    {
        $header = $this->factory->H3();
        $expected = '<h3 ></h3>';

        $this->assertInstanceOf(Container::class, $header);
        $this->assertEquals($expected, $header->generateItem());
    }

    /**
     * @test
     */
    public function H4(): void
    {
        $header = $this->factory->H4();
        $expected = '<h4 ></h4>';

        $this->assertInstanceOf(Container::class, $header);
        $this->assertEquals($expected, $header->generateItem());
    }

    /**
     * @test
     */
    public function H5(): void
    {
        $header = $this->factory->H5();
        $expected = '<h5 ></h5>';

        $this->assertInstanceOf(Container::class, $header);
        $this->assertEquals($expected, $header->generateItem());
    }
}
