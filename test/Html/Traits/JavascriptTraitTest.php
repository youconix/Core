<?php

namespace youconix\Core\Test\Html\Traits;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Traits\JavascriptTrait;

class JavascriptTraitTest extends TestCase
{
    /**
     * @var JavascriptTrait
     */
    private $javascript;

    public function setUp(): void
    {
        $this->javascript = new class
        {
            use JavascriptTrait;

            public function generateItem()
            {
                return $this->parseJavaScript();
            }
        };
    }

    /**
     * @test
     */
    public function setID(): void
    {
        $id = 'parser1';

        $this->javascript->setID('fake-id');
        $this->javascript->setID($id);

        $expected = 'id="' . $id . '" ';
        $this->assertEquals($expected, $this->javascript->generateItem());
    }

    /**
     * @test
     */
    public function setData(): void
    {
        $data = [
            'item1' => 'value1',
            'item2' => 'value2'
        ];

        $this->javascript->setData('item1', $data['item1']);
        $this->javascript->setData('item2', $data['item2']);

        $expected = 'data-item1 = "' . $data['item1'] . '" data-item2 = "' . $data['item2'] . '" ';
        $this->assertEquals($expected, $this->javascript->generateItem());
    }

    /**
     * @test
     */
    public function setEvent(): void
    {
        $data = [
            'event1' => 'value1',
            'event2' => 'value2'
        ];

        $this->javascript->setEvent('event1', $data['event1']);
        $this->javascript->setEvent('event2', $data['event2']);

        $expected = 'onEvent1 = "' . $data['event1'] . '" onEvent2 = "' . $data['event2'] . '" ';
        $this->assertEquals($expected, $this->javascript->generateItem());
    }
}