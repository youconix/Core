<?php

namespace youconix\Core\Test\Html\Form;

use youconix\Core\Html\Form\Form;
use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    /**
     * @test
     */
    public function noMultiData(): void
    {
        $link = 'http://example.com';
        $method = 'POST';
        $result = (new Form($link, $method))->generateItem();

        $expected = '<form action="' . $link . '" method="' . $method . '" >' . PHP_EOL . PHP_EOL . '</form>';

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function multiData(): void
    {
        $link = 'http://example.com';
        $method = 'POST';
        $result = (new Form($link, $method, true))->generateItem();

        $expected = '<form action="' . $link . '" method="' . $method . '" enctype="multipart/form-data" >' . PHP_EOL . PHP_EOL . '</form>';

        $this->assertEquals($expected, $result);
    }
}
