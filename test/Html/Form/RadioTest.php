<?php

namespace youconix\Core\Test\Html\Form;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Form\Radio;
use youconix\Core\Html\Html;

class RadioTest extends TestCase
{
    /** @var Radio */
    private $radio;

    /** @var string */
    private $name = 'test';

    public function setUp(): void
    {
        $factory = new Html();
        $this->radio = $factory->radio($this->name);
    }

    /**
     * @test
     */
    public function radioChecked(): void
    {
        $this->radio->setChecked();

        $expected = '<input type="radio" name="' . $this->name . '" checked="checked" >';
        $this->assertEquals($expected, $this->radio->generateItem());
    }

    /**
     * @test
     */
    public function radioCheckedWithValue(): void
    {
        $value = 'radio value';
        $this->radio->setValue($value)->setChecked();

        $expected = '<input type="radio" name="' . $this->name . '" value="' . $value . '" checked="checked" >';
        $this->assertEquals($expected, $this->radio->generateItem());
    }

    /**
     * @test
     */
    public function radioNotChecked(): void
    {
        $expected = '<input type="radio" name="' . $this->name . '" >';
        $this->assertEquals($expected, $this->radio->generateItem());
    }
}
