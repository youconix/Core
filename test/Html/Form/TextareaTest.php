<?php

namespace youconix\Core\Test\Html\Form;

use youconix\Core\Html\Form\Textarea;
use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Html;

class TextareaTest extends TestCase
{
    /** @var Textarea */
    private $textArea;

    /** @var string */
    private $name = 'test';

    public function setUp(): void
    {
        $factory = new Html();
        $this->textArea = $factory->textarea($this->name);
    }

    /**
     * @test
     */
    public function setRows(): void
    {
        $rows = 10;
        $this->textArea->setRows($rows);
        $result = $this->textArea->generateItem();

        $expected = '<textarea rows="' . $rows . '"  name="' . $this->name . '" ></textarea>';
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function setContent(): void
    {
        $content = 'test content';
        $this->textArea->setContent($content);
        $result = $this->textArea->generateItem();

        $expected = '<textarea  name="' . $this->name . '" >' . $content . '</textarea>';
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function generateItem(): void
    {
        $result = $this->textArea->generateItem();

        $expected = '<textarea  name="' . $this->name . '" ></textarea>';
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function addContent(): void
    {
        $content = 'test content';
        $moreContent = ' even more content';
        $this->textArea->setContent($content);
        $this->textArea->addContent($moreContent);
        $result = $this->textArea->generateItem();

        $expected = '<textarea  name="' . $this->name . '" >' . $content . $moreContent . '</textarea>';
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function setCols(): void
    {
        $cols = 5;
        $this->textArea->setCols($cols);
        $result = $this->textArea->generateItem();

        $expected = '<textarea cols="' . $cols . '"  name="' . $this->name . '" ></textarea>';
        $this->assertEquals($expected, $result);
    }
}
