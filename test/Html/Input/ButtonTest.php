<?php

namespace youconix\Core\Test\Html\Input;

use youconix\Core\Html\Html;
use PHPUnit\Framework\TestCase;

class ButtonTest extends TestCase
{
    /** @var Html */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new Html();
    }

    /**
     * @dataProvider inputTypes
     * @param string $name
     * @param string $type
     * @param string $expected
     * @test
     */
    public function setData(string $name, string $type, string $expected): void
    {
        $input = $this->factory->button('', $name, $type);

        $this->assertEquals($expected, $input->generateItem());
    }

    /**
     * @test
     */
    public function noName(): void
    {
        $value = 'test value';
        $input = $this->factory->button($value, '', 'button');

        $expected = '<input type="button"  value="' . $value . '">';
        $this->assertEquals($expected, $input->generateItem());
    }

    /**
     * @test
     */
    public function invalidType(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->factory->button('', 'test name', 'text');
    }

    /**
     * @test
     */
    public function setValue(): void
    {
        $name = 'test input';
        $value = 'test value';
        $input = $this->factory->button($value, $name, 'submit');

        $expected = '<input type="submit" name="' . $name . '" value="' . $value . '">';
        $this->assertEquals($expected, $input->generateItem());
    }

    /**
     * @return array
     */
    public function inputTypes(): array
    {
        $name = 'test button';

        return [
            [$name, 'button', '<input type="button" name="' . $name . '" value="">'],
            [$name, 'submit', '<input type="submit" name="' . $name . '" value="">'],
            [$name, 'reset', '<input type="reset" name="' . $name . '" value="">']
        ];
    }
}
