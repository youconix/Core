<?php

namespace youconix\Core\Test\Html\Input;

use youconix\Core\Html\Html;
use youconix\Core\Html\Input\Date;
use youconix\Core\Html\Input\Range;
use PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{
    /**
     * @var Date
     */
    private $date;

    /**
     * @var string
     */
    private $name = 'test date';

    /**
     * @var string
     */
    private $value = '2019-01-01';

    public function setUp(): void
    {
        $factory = new Html();
        $this->date = $factory->date($this->name, $this->value);
    }

    /**
     * @test
     */
    public function setValue(): void
    {
        $value = '2019-01-31';

        $this->date->setValue($value);
        $expected = '<input type="date" name="' . $this->name . '" value="' . $value . '" >' . PHP_EOL;

        $this->assertEquals($expected, $this->date->generateItem());
    }
}
