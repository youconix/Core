<?php

namespace youconix\Core\Test\Html\Input;

use youconix\Core\Html\Html;
use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{
    /** @var Html */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new Html();
    }

    /**
     * @dataProvider inputTypes
     * @param string $name
     * @param string $type
     * @param string $expected
     * @test
     */
    public function setData(string $name, string $type, string $expected): void
    {
        $input = $this->factory->input($name, $type);

        $this->assertEquals($expected, $input->generateItem());
    }

    /**
     * @test
     */
    public function invalidType(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->factory->input('test name', 'button');
    }

    /**
     * @test
     */
    public function setValue(): void
    {
        $name = 'test input';
        $value = 'test value';
        $input = $this->factory->input($name, 'text', $value);

        $expected = '<input type="text" name="' . $name . '"  value="' . $value . '">';
        $this->assertEquals($expected, $input->generateItem());
    }

    /**
     * @return array
     */
    public function inputTypes(): array
    {
        $name = 'test input';

        return [
            [$name, 'text', '<input type="text" name="' . $name . '"  value="">'],
            [$name, 'hidden', '<input type="hidden" name="' . $name . '"  value="">'],
            [$name, 'password', '<input type="password" name="' . $name . '"  value="">'],
            [$name, 'search', '<input type="search" name="' . $name . '"  value="">'],
            [$name, 'email', '<input type="email" name="' . $name . '"  value="">'],
            [$name, 'url', '<input type="url" name="' . $name . '"  value="">'],
            [$name, 'tel', '<input type="tel" name="' . $name . '"  value="">'],
            [$name, 'date', '<input type="date" name="' . $name . '"  value="">'],
            [$name, 'month', '<input type="month" name="' . $name . '"  value="">'],
            [$name, 'week', '<input type="week" name="' . $name . '"  value="">'],
            [$name, 'time', '<input type="time" name="' . $name . '"  value="">'],
            [$name, 'color', '<input type="color" name="' . $name . '"  value="">']
        ];
    }
}
