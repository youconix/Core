<?php

namespace youconix\Core\Test\Html\Input;

use youconix\Core\Html\Html;
use youconix\Core\Html\Input\Date;
use youconix\Core\Html\Input\Datetime;
use youconix\Core\Html\Input\Range;
use PHPUnit\Framework\TestCase;

class DatetimeTest extends TestCase
{
    /**
     * @var Html
     */
    private $factory;

    /**
     * @var string
     */
    private $name = 'test datetime';

    /**
     * @var string
     */
    private $value = '2019-01-01 12:00:00';

    public function setUp(): void
    {
        $this->factory = new Html();
    }

    /**
     * @test
     */
    public function setValue(): void
    {
        $value = '2019-01-31 12:00:00';
        $datetime = $this->factory->datetime($this->name, $this->value);
        $datetime->setValue($value);

        $expected = '<input type="datetime" name="' . $this->name . '" value="' . $value . '" >' . PHP_EOL;

        $this->assertEquals($expected, $datetime->generateItem());
    }

    /**
     * @test
     */
    public function setValueLocal(): void
    {
        $value = '2019-01-31 12:00:00';
        $datetime = $this->factory->datetime($this->name, $this->value, true);
        $datetime->setValue($value);

        $expected = '<input type="datetime-local" name="' . $this->name . '" value="' . $value . '" >' . PHP_EOL;

        $this->assertEquals($expected, $datetime->generateItem());
    }
}
