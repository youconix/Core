<?php

namespace youconix\Core\Test\Html\Input;

use youconix\Core\Html\Html;
use youconix\Core\Html\Input\Range;
use PHPUnit\Framework\TestCase;

class RangeTest extends TestCase
{
    /**
     * @var Range
     */
    private $range;

    /**
     * @var string
     */
    private $name = 'test range';

    /**
     * @var string
     */
    private $value = 'test value';

    public function setUp(): void
    {
        $factory = new Html();
        $this->range = $factory->range($this->name, $this->value);
    }

    /**
     * @test
     */
    public function setValue(): void
    {
        $value = 'age';

        $this->range->setValue($value);
        $expected = '<input type="range" name="' . $this->name . '" value="' . $value . '" >' . PHP_EOL;

        $this->assertEquals($expected, $this->range->generateItem());
    }

    /**
     * @test
     */
    public function setMinimum(): void
    {
        $mimimum = 5;

        $this->range->setMinimum($mimimum);
        $expected = '<input type="range" name="' . $this->name . '" value="' . $this->value . '" min="' . $mimimum . '" >' . PHP_EOL;

        $this->assertEquals($expected, $this->range->generateItem());
    }

    /**
     * @test
     */
    public function setMaximum(): void
    {
        $maximum = 5;

        $this->range->setMaximum($maximum);
        $expected = '<input type="range" name="' . $this->name . '" value="' . $this->value . '" max="' . $maximum . '" >' . PHP_EOL;

        $this->assertEquals($expected, $this->range->generateItem());
    }
}
