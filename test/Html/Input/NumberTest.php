<?php

namespace youconix\Core\Test\Html\Input;

use youconix\Core\Html\Html;
use youconix\Core\Html\Input\Number;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    /**
     * @var Number
     */
    private $number;

    /**
     * @var string
     */
    private $name = 'test number';

    /**
     * @var string
     */
    private $value = 'test value';

    public function setUp(): void
    {
        $factory = new Html();
        $this->number = $factory->number($this->name, $this->value);
    }

    /**
     * @test
     */
    public function setStepFloat(): void
    {
        $step = 0.5;

        $this->number->setStep($step);
        $expected = '<input type="number" name="' . $this->name . '" value="' . $this->value . '" step="' . $step . '" >' . PHP_EOL;

        $this->assertEquals($expected, $this->number->generateItem());
    }

    /**
     * @test
     */
    public function setStepInt(): void
    {
        $step = 5;

        $this->number->setStep($step);
        $expected = '<input type="number" name="' . $this->name . '" value="' . $this->value . '" step="' . $step . '" >' . PHP_EOL;

        $this->assertEquals($expected, $this->number->generateItem());
    }

    /**
     * @test
     */
    public function setStepInvalid(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $step = 'not-a-number';

        $this->number->setStep($step);
    }
}
