<?php

namespace Youconix\Core\Test\Type;

use youconix\Core\Type\StringParser;
use PHPUnit\Framework\TestCase;

class StringParserTest extends TestCase
{
    /** @var StringParser */
    private $uut;

    /** @var string */
    private $value;

    public function setUp()
    {
        $this->value = 'String parser test Value';

        $this->uut = StringParser::fromString($this->value);
    }

    /**
     * @test
     */
    public function firstCharacterShouldBeLowercase(): void
    {
        $expected = lcfirst($this->value);

        $this->assertEquals($expected, $this->uut->firstToLowercase());
    }

    /**
     * @test
     */
    public function shouldFindPosition(): void
    {
        $this->assertEquals(14, $this->uut->findPosition('test value'));
        $this->assertEquals(0, $this->uut->findPosition('String parser'), true);
    }

    /**
     * @test
     */
    public function shouldNotFindPosition(): void
    {
        $this->assertEquals(-1, $this->uut->findPosition('Test Value', true));
        $this->assertEquals(-1, $this->uut->findPosition('string parsers'));
    }

    /**
     * @test
     */
    public function shouldContainText(): void
    {
        $this->assertTrue($this->uut->contains('test value'));
        $this->assertTrue($this->uut->contains('String parser'), true);
    }

    /**
     * @test
     */
    public function shouldNotContainText(): void
    {
        $this->assertFalse($this->uut->contains('Test Value', true));
        $this->assertFalse($this->uut->contains('String parsers'));
    }

    /**
     * @test
     */
    public function shouldEndWith(): void
    {
        $this->assertTrue($this->uut->endsWith('value'));
        $this->assertTrue($this->uut->endsWith('Value', true));
    }

    /**
     * @test
     */
    public function shouldNotEndWith(): void
    {
        $this->assertFalse($this->uut->endsWith('values'));
        $this->assertFalse($this->uut->endsWith('value', true));
    }

    /**
     * @test
     */
    public function shouldFormat(): void
    {
        $uut = StringParser::fromString('232312.5632');

        $this->assertEquals('232,313', $uut->format());
        $this->assertEquals('232,312.5632', $uut->format(4));
        $this->assertEquals('232.312,5632', $uut->format(4, ',', '.'));
    }

    /**
     * @test
     */
    public function shouldReturnLength(): void
    {
        $expected = mb_strlen($this->value);

        $this->assertEquals($expected, $this->uut->size());
    }

    /**
     * @test
     */
    public function shouldReplaceText(): void
    {
        $expected = 'String parser values';
        $result = $this->uut->replace('test Value', 'values');

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function shouldReplacePart(): void
    {
        $expected = 'String value';
        $result =  $this->uut->substringReplace('v', 7,13);

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function shouldReverseEntities(): void
    {
        $value = 'mali&euml;n';
        $expected = 'maliën';

        $result = StringParser::fromString($value)->reverseEntities();

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function shouldReverseSpecialCharacters(): void
    {
        $value = 'test &amp; results';
        $expected = 'test & results';

        $result = StringParser::fromString($value)->reverseSpecialCharacters();

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function shouldSplitOnCharacter(): void
    {
        $expected = [
            'String',
            'parser',
            'test',
            'Value'
        ];

        $result = $this->uut->split(' ');

        $this->assertEquals($expected, $result);
    }

    public function testToSpecialChars()
    {

    }

    public function testTrim()
    {

    }

    public function testStartsWith()
    {

    }

    public function testSubstringReplace()
    {

    }

    public function testTrimRight()
    {

    }

    public function testFormat()
    {

    }

    public function testReverseEntities()
    {

    }

    public function testTrimLeft()
    {

    }

    public function testSubstring()
    {

    }

    public function testSize()
    {

    }

    public function testReverseSpecialChars()
    {

    }

    public function testShouldLowerCaseValue(): void
    {
        $expected = strtolower($this->value);

        $this->assertEquals($expected, $this->uut->toLowercase());
    }

    public function testToUppercase()
    {

    }

    public function testSplitSize()
    {

    }

    public function testToEntities()
    {

    }

    public function testSplit()
    {

    }

    public function testEndsWith()
    {

    }

    public function testReplace()
    {

    }

    public function testFirstToUppercase()
    {

    }
}
