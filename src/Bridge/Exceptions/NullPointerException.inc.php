<?php

namespace youconix\Core\Bridge\Exceptions;

/** 
 * @deprecated
 */
class NullPointerException extends GeneralException{
    public function __construct($s_message){
        parent::__construct($s_message);
    }
}
