<?php

namespace youconix\Core\Bridge\Exceptions;

/**
 * @deprecated
 */
class DateException extends GeneralException {
    public function __construct($s_message){
        $this->message  = $s_message;
    }
}