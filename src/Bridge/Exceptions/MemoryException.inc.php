<?php

namespace youconix\Core\Bridge\Exceptions;

/** 
 * @deprecated
 */
class MemoryException extends GeneralException {
    public function __construct($s_message){
        parent::__construct($s_message);
    }
}
