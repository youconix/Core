<?php

namespace youconix\Core\Database;

interface Builder
{
    /**
     * Creates the builder
     *
     * @param DAL $service_Database The DAL
     */
    public function __construct(DAL $service_Database);

    /**
     * Shows the tables in the current database
     */
    public function showTables();

    /**
     * Shows the databases that the user has access to
     */
    public function showDatabases();

    /**
     * Creates a select statement
     *
     * @param        String $s_table The table name
     * @param        String $s_fields The field names sepperated with a ,
     */
    public function select($s_table, $s_fields);

    /**
     * Creates a insert statement
     *
     * @param        String $s_table The table name
     * @param        array $a_fields The field names, also accepts a single value
     * @param        array $a_types The value types : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param        array $a_values The values, also accepts a single value
     */
    public function insert($s_table, $a_fields, $a_types, $a_values);

    /**
     * Creates a update statement
     *
     * @param        String $s_table The table name
     * @param        array $a_fields The field names, also accepts a single value
     * @param        array $a_types The value types : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param        array $a_values The values, also accepts a single value
     */
    public function update($s_table, $a_fields, $a_types, $a_values);

    /**
     * Creates a delete statement
     *
     * @param    String $s_table The table name
     */
    public function delete($s_table);

    /**
     * Returns the create table generation class
     *
     * @param String $s_table The table name
     * @param Boolean $bo_dropTable Set to true to drop the given table before creating it
     * @return Create The create table generation class
     */
    public function getCreate($s_table, $bo_dropTable);

    /**
     * Adds a inner join between 2 tables
     *
     * @param        String $s_table The table name
     * @param        String $s_field1 The field from the first table
     * @param        String $s_field2 The field from the second table
     */
    public function innerJoin($s_table, $s_field1, $s_field2);

    /**
     * Adds a outer join between 2 tables
     *
     * @param        String $s_table The table name
     * @param        String $s_field1 The field from the first table
     * @param        String $s_field2 The field from the second table
     */
    public function outerJoin($s_table, $s_field1, $s_field2);

    /**
     * Adds a left join between 2 tables
     *
     * @param        String $s_table The table name
     * @param        String $s_field1 The field from the first table
     * @param        String $s_field2 The field from the second table
     */
    public function leftJoin($s_table, $s_field1, $s_field2);

    /**
     * Adds a right join between 2 tables
     *
     * @param        String $s_table The table name
     * @param        String $s_field1 The field from the first table
     * @param        String $s_field2 The field from the second table
     */
    public function rightJoin($s_table, $s_field1, $s_field2);

    /**
     * Returns the where generation class
     *
     * @return Where    The where generation class
     */
    public function getWhere();

    /**
     * Adds a limitation to the query statement
     * Only works on select, update and delete statements
     *
     * @param        int $i_limit The limitation of records
     * @param        int $i_offset The offset to start from, default 0 (first record)
     */
    public function limit($i_limit, $i_offset = 0);

    /**
     * Groups the results by the given field
     *
     * @param        String $s_field The field
     */
    public function group($s_field);

    /**
     * Returns the having generation class
     *
     * @return Having        The having generation class
     */
    public function getHaving();

    /**
     * Orders the records in the given order
     *
     * @param        String $s_field1 The first field to order on
     * @param        String $s_ordering1 The ordering method (ASC|DESC)
     * @param        String $s_field2 The second field to order on, optional
     * @param        String $s_ordering2 The ordering method (ASC|DESC), optional
     */
    public function order($s_field1, $s_ordering1 = 'ASC', $s_field2 = '', $s_ordering2 = 'ASC');

    /**
     * Return the total amount statement for the given field
     *
     * @param    String $s_field The field name
     * @param    String $s_alias The alias, default the field name
     * @return String        The statement
     */
    public function getSum($s_field, $s_alias = '');

    /**
     * Return the maximun value statement for the given field
     *
     * @param    String $s_field The field name
     * @param    String $s_alias The alias, default the field name
     * @return String        The statement
     */
    public function getMaximun($s_field, $s_alias = '');

    /**
     * Return the minimun value statement for the given field
     *
     * @param String $s_field The field name
     * @param String $s_alias The alias, default the field name
     * @return String        The statement
     */
    public function getMinimun($s_field, $s_alias = '');

    /**
     * Return the average value statement for the given field
     *
     * @param String $s_field The field name
     * @param String $s_alias The alias, default the field name
     * @return String        The statement
     */
    public function getAverage($s_field, $s_alias = '');

    /**
     * Return statement for counting the number of records on the given field
     *
     * @param String $s_field The field name
     * @param String $s_alias The alias, default the field name
     * @return String        The statement
     */
    public function getCount($s_field, $s_alias = '');

    /**
     * Returns the query result
     *
     * @return DAL        The query result as a database object
     */
    public function getResult();

    /**
     * Builds the query
     */
    public function render();

    /**
     * Starts a new transaction
     *
     * @throws DBException    If a transaction is allready active
     */
    public function transaction();

    /**
     * Commits the current transaction
     *
     * @throws DBException    If no transaction is active
     */
    public function commit();

    /**
     * Rolls the current transaction back
     *
     * @throws DBException    If no transaction is active
     */
    public function rollback();

    /**
     * Returns the DAL
     *
     * @return DAL  The DAL
     */
    public function getDatabase();
}