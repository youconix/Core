<?php

namespace youconix\Core\Database;

use youconix\Core\Bridge\Exceptions\DBException;

interface DAL
{
    /**
     * Destructor
     */
    public function __destruct();

    /**
     * Connects to the database with the preset login data
     */
    public function defaultConnect();

    /**
     * Checks if the given connection-data is correct
     *
     * @static
     * @param   string $s_username
     * @param   string $s_password
     * @param   string $s_database
     * @param   string $s_host
     * @param   int $i_port
     * @return  boolean True if the data is correct, otherwise false
     */
    public static function checkLogin($s_username, $s_password, $s_database, $s_host = '127.0.0.1', $i_port = -1);

    /**
     * Connects to the set database
     *
     * @param   string $s_username
     * @param   string $s_password
     * @param   string $s_database
     * @param   string $s_host
     * @param   int $i_port
     * @throws DBException if the connection failes
     */
    public function connection($s_username, $s_password, $s_database, $s_host = '127.0.0.1', $i_port = -1);

    /**
     * Closes the connection to the database
     */
    public function connectionEnd();

    /**
     * Returns the ID generated by a INSERT-command
     *
     * @return		int The generated id
     */
    public function getId();

    /**
     * Returns numbers of rows affected generated by a UPDATE or DELETE command
     *
     * @return		Int The requested id
     */
    public function affected_rows();

    /**
     * Returns or there is a connection to the database
     *
     * @return		boolean True if there is a connection with the DB, false if is not
     */
    public function isConnected();

    /**
     * Excequetes the given query on the selected database
     *
     * @para            String  $s_query    The query to excequte
     * @throws          Exception when the query failes
     */
    public function query($s_query);

    /**
     * Excequetes the given query on the selected database with binded parameters
     *
     * @param	 string	$s_query	The query to excequte
     * @param	 array	$a_types	The value types : i (int) ,d (double) ,s (string) or b (blob)
     * @param	 array  $a_values	The values
     * @throws  Exception if the arguments are illegal
     * @throws  DBException when the query failes
     */
    public function queryBinded($s_query, $a_types, $a_values);

    /**
     * Returns the number of results from the last excequeted query
     *
     * @return          int The number of results
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function num_rows();

    /**
     * Returns the result from the query with the given row and field
     *
     * @param           int     The row
     * @param           string  The field
     * @return          string  The content of the requested result-field
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function result($i_row, $s_field);

    /**
     * Returns the results of the query in a numeric array
     *
     * @return          array	The data-set
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function fetch_row();

    /**
     * Returns the results of the query in a associate and numeric array
     *
     * @return          array	The data-set
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function fetch_array();

    /**
     * Returns the results of the query in a associate array
     *
     * @return          array	The data-set
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function fetch_assoc();

    /**
     * Returns the results of the query in a associate array with the given field as counter-key
     *
     * @param           string  The field that is the counter-key
     * @return			 array	The data-set sorted on the given key
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function fetch_assoc_key($s_key);

    /**
     * Returns the results of the query as a object-array
     *
     * @return          object	The data-set
     * @throws          Exception when no SELECT-query was excequeted
     */
    public function fetch_object();

    /**
     * Escapes the given data for save use in queries
     *
     * @param           string  $s_data  The data that need to be escaped
     * @return          string  The escaped data
     */
    public function escape_string($s_data);

    /**
     * Starts a new transaction
     *
     * @throws DBException	If a transaction is allready active
     */
    public function transaction();

    /**
     * Commits the current transaction
     *
     * @throws DBException	If no transaction is active
     */
    public function commit();

    /**
     * Rolls the current transaction back
     *
     * @throws DBException	If no transaction is active
     */
    public function rollback();

    /**
     * Analyses the given table
     *
     * @param string $s_table		The table name
     * @return boolean	True if the table is OK, otherwise false
     */
    public function analyse($s_table);

    /**
     * Repairs the given table
     *
     * @param string $s_table		The table name
     * @return boolean	True if the table repair succeeded, otherwise false
     */
    public function repair($s_table);

    /**
     * Optimizes the given table
     *
     * @param string $s_table		The table name
     */
    public function optimize($s_table);

    /**
     * Changes the active database to the given one
     *
     * @param	string	$s_database	The new database
     * @throws	DBException if the new databases does not exist or no access
     */
    public function useDB($s_database);

    /**
     * Checks if a database exists and if the user has access to it
     *
     * @param	string	$s_database	The database
     * @return  boolean	True if the database exists, otherwise false
     */
    public function databaseExists($s_database);
}