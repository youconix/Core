<?php

namespace youconix\Core\Database;

use youconix\Core\Bridge\Exceptions\DBException;

interface Having {
    /**
     * Resets the class Having
     * @deprecated
     */
    public function reset();

    /**
     * Adds fields with an and relation
     *
     * @param array 	$a_fields		The fields,also accepts a single value
     * @param		array	$a_types	The value types : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param	 array	$a_values		The values, also accepts a single value
     * @param array		$a_keys			The keys (=|<>|<|>|LIKE|IN|BETWEEN), also accepts a single value. leave empty for =
     * @throws DBException		If the key is invalid
     * @deprecated
     */
    public function addAnd($a_fields,$a_types,$a_values,$a_keys);

    /**
     * Adds fields with an or relation
     *
     * @param array 	$a_fields		The fields,also accepts a single value
     * @param		array	$a_types	The value types : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param	 array	$a_values		The values, also accepts a single value
     * @param array		$a_keys			The keys (=|<>|<|>|LIKE|IN|BETWEEN), also accepts a single value. leave empty for =
     * @throws DBException		If the key is invalid
     * @deprecated
     */
    public function addOr($a_fields,$a_types,$a_values,$a_keys);

    /**
     * Starts a sub having part
     * @deprecated
     */
    public function startSubHaving();

    /**
     * Ends a sub having part
     * @deprecated
     */
    public function endSubHaving();

    /**
     * Renders the having
     *
     * @return array		The having
     * @deprecated
     */
    public function render();
}