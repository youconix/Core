<?php

namespace youconix\Core\Database;

use youconix\Core\Bridge\Exceptions\DBException;

interface Create
{
    /**
     * @deprecated
     */
    public function reset();

    /**
     * Creates a table
     *
     * @param    String $s_table The table name
     * @param    Boolean $bo_dropTable Set to true to drop the given table before creating it
     * @deprecated
     */
    public function setTable($s_table, $bo_dropTable = false);

    /**
     * Adds a field to the create statement
     *
     * @param String $s_field The field name
     * @param String $s_type The field type (database type!)
     * @param int $i_length The length of the field, only for length fields
     * @param String $s_default The default value
     * @param bool $bo_signed Set to true for signed value, default unsigned
     * @param bool $bo_null Set to true for NULL allowed
     * @param bool $bo_autoIncrement Set to true for auto increment
     * @deprecated
     */
    public function addRow(
        $s_field,
        $s_type,
        $i_length,
        $s_default = '',
        $bo_signed = false,
        $bo_null = false,
        $bo_autoIncrement = false
    );

    /**
     * Adds an enum field to the create stament
     *
     * @param String $s_field The field name
     * @param array $a_values The values
     * @param String $s_default The default value
     * @param bool $bo_null Set to true for NULL allowed
     * @deprecated
     */
    public function addEnum($s_field, $a_values, $s_default, $bo_null = false);

    /**
     * Adds a set field to the create stament
     *
     * @param String $s_field The field name
     * @param array $a_values The values
     * @param String $s_default The default value
     * @param bool $bo_null Set to true for NULL allowed
     * @deprecated
     */
    public function addSet($s_field, $a_values, $s_default, $bo_null = false);

    /**
     * Adds a primary key to the given field
     *
     * @param String $s_field The field name
     * @throws DBException If the field is unknown or if the primary key is allready set
     * @deprecated
     */
    public function addPrimary($s_field);

    /**
     * Adds a index to the given field
     *
     * @param String $s_field The field name
     * @throws DBException If the field is unknown
     * @deprecated
     */
    public function addIndex($s_field);

    /**
     * Sets the given fields as unique
     *
     * @param String $s_field The field name
     * @throws DBException If the field is unknown
     * @deprecated
     */
    public function addUnique($s_field);

    /**
     * Sets full text search on the given field
     *
     * @param String $s_field The field name
     * @throws DBException If the field is unknown
     * @throws DBException If the field type is not VARCHAR and not TEXT.
     * @deprecated
     */
    public function addFullTextSearch($s_field);

    /**
     * Returns the drop table setting
     *
     * @return String    The drop table command. Empty string for not dropping
     * @deprecated
     */
    public function getDropTable();

    /**
     * Creates the query
     *
     * @return String        The query
     * @deprecated
     */
    public function render();
}