<?php

namespace youconix\Core\Database;

use youconix\Core\Bridge\Exceptions\DBException;

/**
 * @deprecated
 */
class Where_Mysqli extends QueryConditions_Mysqli implements \core\services\Where
{
    protected $a_builder;

    /**
     * Resets the class Where_Mysqli
     * @deprecated
     */
    public function reset()
    {
        trigger_error('Class Where_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        parent::reset();
        $this->a_builder = null;
    }

    /**
     * Starts a sub where part
     * @deprecated
     */
    public function startSubWhere()
    {
        trigger_error('Class Where_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query .= '(';

        return $this;
    }

    /**
     * Ends a sub where part
     * @deprecated
     */
    public function endSubWhere()
    {
        trigger_error('Class Where_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query .= ')';

        return $this;
    }

    /**
     * Adds a sub query
     *
     * @param  Builder $obj_builder The builder object
     * @param  String $s_field The field
     * @param  string $s_key The key (=|<>|LIKE|IN|BETWEEN)
     * @param  string $s_command The command (AND|OR)
     * @throws DBException        If the key is invalid
     * @throws DBException        If the command is invalid
     * @return $this
     * @deprecated
     */
    public function addSubQuery($obj_builder, $s_field, $s_key, $s_command)
    {
        trigger_error('Class Where_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!($obj_builder instanceof Builder)) {
            throw new DBException("Can only add object of the type Builder.");
        }

        if (!array_key_exists($s_key, $this->a_keys)) {
            throw new DBException('Unknown where key ' . $s_key . '.');
        }

        $s_command = strtoupper($s_command);
        if (!in_array($s_command, array('OR', 'AND'))) {
            throw new DBException('Unknown where command ' . $s_command . '.  Only AND & OR are supported.');
        }

        $this->a_builder = array(
            'object' => $obj_builder,
            'field' => $s_field,
            'key' => $s_key,
            'command' => $s_command
        );

        return $this;
    }

    /**
     * Renders the where
     *
     * @return array        The where
     * @deprecated
     */
    public function render()
    {
        trigger_error('Class Where_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (empty($this->s_query)) {
            return null;
        }

        if (!is_null($this->a_builder)) {
            $obj_builder = $this->a_builder['object']->render();
            $this->s_query .= $this->a_builder['command'] . ' ' . $this->a_builder['field'] . ' ' . $this->a_builder['key'] . ' (' . $obj_builder['query'] . ')';
            $this->a_values[] = $obj_builder['values'];
            $this->a_types[] = $obj_builder['types'];
        }

        return array('where' => ' WHERE ' . $this->s_query, 'values' => $this->a_values, 'types' => $this->a_types);
    }
}