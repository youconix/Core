<?php

namespace youconix\Core\Bridge\Classes;

/**
 * @deprecated
 */
class StringLegancy
{
    private $s_content = '';
    private $i_size = 0;

    /**
     * @param string $s_value The value, optional
     */
    public function __construct($s_value = '')
    {
        $this->set($s_value);
    }

    /**
     * Sets the value, overwrites any existing value
     *
     * @param string $s_value The value
     * @deprecated
     */
    public function set($s_value)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        $this->s_content = $s_value;
        $this->i_size = strlen($s_value);
    }

    /**
     * Appends the given value to the existing value
     *
     * @param string $s_value The value
     * @deprecated
     */
    public function append($s_value)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        $this->s_content .= $s_value;
        $this->i_size += strlen($s_value);
    }

    /**
     * Returns the length
     *
     * @return int    The length
     * @deprecated
     */
    public function length()
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        return $this->i_size;
    }

    /**
     * Returns the value
     *
     * @return string    The value
     * @deprecated
     */
    public function value()
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        return $this->s_content;
    }

    /**
     * Checks if the value starts with the given text
     *
     * @param string $s_text The text to search on
     * @return boolean    True if the value starts with the given text
     * @deprecated
     */
    public function startsWith($s_text)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        if (substr($this->s_content, 0, strlen($s_text)) == $s_text) {
            return true;
        }

        return false;
    }

    /**
     * Checks if the value ends with the given text
     *
     * @param string $s_text The text to search on
     * @return boolean    True if the value ends with the given text
     * @deprecated
     */
    public function endsWith($s_text)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        if (substr($this->s_content, (strlen($s_text) * -1)) == $s_text) {
            return true;
        }

        return false;
    }

    /**
     * Checks if the value contains the given text
     *
     * @param string $s_text The text to search on
     * @param boolean $bo_caseSensitive Set to false to search case insensitive
     * @return bool
     * @deprecated
     */
    public function contains($s_text, $bo_caseSensitive = true)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        if ($bo_caseSensitive) {
            $i_pos = stripos($this->s_content, $s_text);
        } else {
            $i_pos = strpos($this->s_content, $s_text);
        }

        if ($i_pos === false) {
            return false;
        }

        return true;
    }

    /**
     * Checks if the value is equal to the given text
     *
     * @param string $s_text The text to check on
     * @return boolean    True if the text is equal
     * @deprecated
     */
    public function equals($s_text)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        return ($this->s_content == $s_text);
    }

    /**
     * Checks if the value is equal to the given text with ignoring the case
     *
     * @param string $s_text The text to check on
     * @return boolean    True if the text is equal
     * @deprecated
     */
    public function equalsIgnoreCase($s_text)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        $s_text = strToLower($s_text);
        $s_check = strToLower($this->s_content);

        return ($s_check == $s_text);
    }

    /**
     * Returns the start position of the given text
     *
     * @param string $s_search The text to search on
     * @param int    The start position or -1 when the text is not found
     * @return int
     * @deprecated
     */
    public function indexOf($s_search)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        $i_pos = stripos($this->s_content, $s_search);
        if ($i_pos === false) {
            $i_pos = -1;
        }

        return $i_pos;
    }

    /**
     * Checks if the string is empty
     *
     * @return    boolean    True if the string is empty
     */
    public function isEmpty()
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        return ($this->i_size == 0);
    }

    /**
     * Removes the spaces at the begin and end
     */
    public function trim()
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        return trim($this->s_content);
    }

    /**
     * Replaces the given search with the given text if the value contains the given search
     *
     * @param string $s_search The text to search on
     * @param string $s_replace The replacement
     */
    public function replace($s_search, $s_replace)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        $this->set(str_replace($s_search, $s_replace, $this->s_content));
    }

    /**
     * Returns the substring from the current value
     *
     * @param int $i_start The start position
     * @param int $i_end The end position
     * @return    string    The substring
     * @deprecated
     */
    public function substring($i_start, $i_end = -1)
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        if ($i_end == -1) {
            return substr($this->s_content, $i_start);
        } else {
            return substr($this->s_content, $i_start, $i_end);
        }
    }

    /**
     * Clones the String object
     *
     * @return String    The clone
     * @deprecated
     */
    public function copy()
    {
        trigger_error('Class String is deprecated. There is no replacement', E_DEPRECATED);

        return clone $this;
    }
}