<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

class Button extends Input
{
    /**
     * @param string $type (button | reset | submit)
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setType(string $type): Input
    {
        $types = ['button', 'reset', 'submit'];

        if (!in_array($type, $types, true)) {
            throw new \InvalidArgumentException(sprintf('Invalid input type %s', $type));
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @param string $name
     */
    protected function setName(string $name): void
    {
        if (!empty($name)) {
            $this->name = 'name="' . $name . '"';
        }
    }
}