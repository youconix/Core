<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

abstract class RangeField implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    protected $tag;

    /**
     * @var string
     */
    protected $minimumValue = '';

    /**
     * @var string
     */
    protected $maximumValue = '';

    /**
     * @var string
     */
    protected $value = '';

    /**
     * @param string $value
     * @return RangeField
     */
    public function setValue(string $value): RangeField
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return  string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{min}', '{max}', '{value}'],
            [$between, $this->minimumValue, $this->maximumValue, $this->value],
            $this->tag
        );
    }
}