<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

class Date extends RangeField
{
    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->tag = '<input type="date" name="' . $name . '" value="{value}" {min}{max}{between}>'.PHP_EOL;
    }

    /**
     * Sets the minimun date
     *
     * @param string minimumDate
     * @return Date
     */
    public function setMinimum(string $minimumDate): Date
    {
        $this->minimumValue = 'min="' . $minimumDate . '" ';
        return $this;
    }

    /**
     * Sets the maximun date
     *
     * @param string $maximumDate
     * @return Date
     */
    public function setMaximum(string $maximumDate): Date
    {
        $this->maximumValue = 'max="' . $maximumDate . '" ';
        return $this;
    }
}