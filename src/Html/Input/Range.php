<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Range extends RangeField
{
    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->tag = '<input type="range" name="' . $name . '" value="{value}" {min}{max}{between}>' . PHP_EOL;
    }

    /**
     * Sets the minimun value
     *
     * @param int $minimumValue
     * @return Range
     */
    public function setMinimum(int $minimumValue): Range
    {
        $this->minimumValue = 'min="' . $minimumValue . '" ';
        return $this;
    }

    /**
     * Sets the maximun value
     *
     * @param int $maximumValue
     * @return Range
     */
    public function setMaximum(int $maximumValue): Range
    {
        $this->maximumValue = 'max="' . $maximumValue . '" ';
        return $this;
    }
}