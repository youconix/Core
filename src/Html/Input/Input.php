<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Input implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    protected $tag;

    /** @var string */
    protected $name;

    /** @var string */
    protected $type;

    /** @var string */
    protected $value;

    /**
     * @param string $name
     * @throws \InvalidArgumentException
     */
    public function __construct(string $name)
    {
        $this->tag = '<input type="{type}" {name}{between} value="{value}">';

        $this->setName($name);
    }

    /**
     * @param string $type (text | password | hidden | email | search | email | url | tel | date | month | week | time | color)
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setType(string $type): Input
    {
        $types = [
            'text',
            'hidden',
            'password',
            'search',
            'email',
            'url',
            'tel',
            'date',
            'month',
            'week',
            'time',
            'color',
        ];

        if (!in_array($type, $types, true)) {
            throw new \InvalidArgumentException(sprintf('Invalid input type %s', $type));
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @param string $name
     */
    protected function setName(string $name): void
    {
        $this->name = 'name="' . $name . '" ';
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): Input
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{value}', '{type}', '{name}'],
            [$between, $this->value, $this->type, $this->name],
            $this->tag
        );
    }
}