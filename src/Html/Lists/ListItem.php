<?php
declare(strict_types=1);

namespace youconix\Core\Html\Lists;

use youconix\Core\Html\Container;

class ListItem extends Container
{
    public function __construct()
    {
        parent::__construct('li');
    }
}