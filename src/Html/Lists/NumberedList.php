<?php
declare(strict_types=1);

namespace youconix\Core\Html\Lists;

class NumberedList extends UnNumberedList
{
    /**
     * @param string $numberType 1 | A | a | I | i
     * @throws \InvalidArgumentException
     */
    public function __construct(string $numberType)
    {
        $this->validateType($numberType);

        $this->tag = '<ul type="' . $numberType . '" {between}>' . PHP_EOL . '{items}' . PHP_EOL . '</ul>';
    }

    /**
     * @param string $numberType
     * @throws \InvalidArgumentException
     */
    private function validateType(string $numberType): void
    {
        $valid = ['1', 'A', 'a', 'I', 'i'];

        if (!in_array($numberType, $valid, true)) {
            throw new \InvalidArgumentException(sprintf('Invalid list number type %s', $numberType));
        }
    }
}