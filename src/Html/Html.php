<?php
declare(strict_types=1);

namespace youconix\Core\Html;

use youconix\Core\Html\Form AS Form;
use youconix\Core\Html\Lists AS Lists;
use youconix\Core\Html\Table AS Table;
use youconix\Core\Html\Input AS Input;
use youconix\Core\Html\Header AS Head;

class Html
{
    /**
     * Generates a article object
     *
     * @param string $content
     * @return Container
     */
    public function article(string $content = ''): Container
    {
        return (new Container('article'))->setContent($content);
    }

    /**
     * Generates a audio object
     *
     * @param string $url
     * @param string $type
     * @return Audio
     */
    public function audio(string $url, string $type = 'ogg'): Audio
    {
        return new Audio($url, $type);
    }

    /**
     * Generates a button
     *
     * @param string $value
     * @param string $name leave empty for no name
     * @param string $type (button | reset | submit)
     * @return  Input\Button
     * @throws \InvalidArgumentException
     */
    public function button(string $value, string $name, string $type): Input\Button
    {
        return (new Input\Button($name))->setType($type)->setValue($value);
    }

    /**
     * Generates a canvas object
     *
     * @return Canvas
     */
    public function canvas(): Canvas
    {
        return new Canvas();
    }

    /**
     * Generates a checkbox
     *
     * @param string $name
     * @param string $value
     * @return  Form\Checkbox
     */
    public function checkbox(string $name, string $value = ''): Form\Checkbox
    {
        return (new Form\Checkbox($name))->setValue($value);
    }

    /**
     * Creates a new date field
     *
     * @param string $name
     * @param string $value
     * @return Input\Date
     */
    public function date(string $name, string $value): Input\Date
    {
        return (new Input\Date($name))->setValue($value);
    }

    /**
     * Creates a new date and time field
     *
     * @param string $name
     * @param string $value
     * @param bool $localize
     * @return Input\Datetime
     */
    public function datetime(string $name, string $value, bool $localize = false): Input\Datetime
    {
        return (new Input\Datetime($name, $localize))->setValue($value);
    }

    /**
     * @param string $content
     * @return  Container
     */
    public function div(string $content = ''): Container
    {
        return (new Container('div'))->setContent($content);
    }

    /**
     * @param string $link The url
     * @param string $method get|post
     * @param bool $isMultiData
     * @return  Form\Form
     */
    public function form(string $link, string $method, bool $isMultiData = false): Form\Form
    {
        return new Form\Form($link, $method, $isMultiData);
    }

    /**
     * Generates a header
     *
     * @param int $level The type of header (1|2|3|4|5)
     * @param string $content
     * @return      Container
     * @throws \InvalidArgumentException
     * @deprecated  Use H1, H2, H3, H4 or H5 instead.
     */
    public function header(int $level, string $content): Container
    {
        switch ($level) {
            case 1:
                return $this->H1($content);
            case 2:
                return $this->H2($content);
            case 3:
                return $this->H3($content);
            case 4:
                return $this->H4($content);
            case 5:
                return $this->H5($content);
            default:
                throw new \InvalidArgumentException(sprintf('Unknown header type %d', $level));
        }
    }

    /**
     * @param string $content
     * @return Container
     */
    public function H1(string $content = ''): Container
    {
        return (new Container('h1'))->setContent($content);
    }

    /**
     * @param string $content
     * @return Container
     */
    public function H2(string $content = ''): Container
    {
        return (new Container('h2'))->setContent($content);
    }

    /**
     * @param string $content
     * @return Container
     */
    public function H3(string $content = ''): Container
    {
        return (new Container('h3'))->setContent($content);
    }

    /**
     * @param string $content
     * @return Container
     */
    public function H4(string $content = ''): Container
    {
        return (new Container('h4'))->setContent($content);
    }

    /**
     * @param string $content
     * @return Container
     */
    public function H5(string $content = ''): Container
    {
        return (new Container('h5'))->setContent($content);
    }

    /**
     * Generates a image
     *
     * @param string $url
     * @return  Image
     */
    public function image(string $url): Image
    {
        return new Image($url);
    }

    /**
     * Generates a text input field
     *
     * @param string $name
     * @param string $type (text | password | hidden | email | search | email | url | tel | date | month | week | time | color)
     * @param string $defaultText
     * @return  Input\Input
     * @throws \InvalidArgumentException
     */
    public function input(string $name, string $type, string $defaultText = ''): Input\Input
    {
        return (new Input\Input($name))->setType($type)->setValue($defaultText);
    }

    /**
     * Generates the javascript tags for inpage javascript
     *
     * @param string $javascript
     * @return Head\Javascript
     */
    public function javascript(string $javascript): Head\Javascript
    {
        return (new Head\Javascript())->setJavascript($javascript);
    }

    /**
     * Generates a link to a external javascript file
     *
     * @param string $link
     * @return Head\JavascriptLink
     */
    public function javascriptLink(string $link): Head\JavascriptLink
    {
        return new Head\JavascriptLink($link);
    }

    /**
     * Generates a link for linking to other pages
     *
     * @param string $url
     * @param string $value
     * @return  Link
     */
    public function link(string $url, string $value = ''): Link
    {
        return new Link($url, $value);
    }

    /**
     * @param string $content
     * @return  Lists\ListItem
     * @deprecated Generate a list item from the lists instead
     */
    public function listItem(string $content): Lists\ListItem
    {
        trigger_error('listItem is deprecated. Generate the items from the lists.', E_DEPRECATED);

        return (new Lists\ListItem())->setContent($content);
    }

    /**
     * Generates a meta tag
     *
     * @param string $name
     * @param string $content
     * @param string $scheme
     * @return  Head\Metatag
     */
    public function metatag(string $name, string $content, $scheme = ''): Head\Metatag
    {
        return (new Head\Metatag($name, $content))->setScheme($scheme);
    }

    /**
     * Generates a footer object
     *
     * @param string $content
     * @return Container
     */
    public function pageFooter(string $content = ''): Container
    {
        return (new Container('footer'))->setContent('');
    }

    /**
     * Generates a header object
     *
     * @param string $content
     * @return Container
     */
    public function pageHeader(string $content = ''): Container
    {
        return (new Container('header'))->setContent('');
    }

    /**
     * Generates a paragraph
     *
     * @param string $content
     * @return  Container
     */
    public function paragraph(string $content = ''): Container
    {
        return (new Container('p'))->setContent($content);
    }

    /**
     * Generates a navigation object
     *
     * @param string $content
     * @return Container
     */
    public function navigation(string $content = ''): Container
    {
        return (new Container('nav'))->setContent($content);
    }

    /**
     * Creates a new number field
     *
     * @param string $name
     * @param string $value
     * @return Input\Number
     */
    public function number(string $name, string $value): Input\Number
    {
        return (new Input\Number($name))->setValue($value);
    }

    /**
     * @param string $numberType 1 | A | a | I | i
     * @return Lists\NumberedList
     * @throws \InvalidArgumentException
     */
    public function numberedList(string $numberType = '1'): Lists\NumberedList
    {
        return new Lists\NumberedList($numberType);
    }

    /**
     * Generates a radio button
     *
     * @param string $name
     * @param string $value
     * @return Form\Radio
     */
    public function radio(string $name, string $value = ''): Form\Radio
    {
        return (new Form\Radio($name))->setValue($value);
    }

    /**
     * Creates a range slider
     *
     * @param string $name
     * @param string $value
     * @return Input\Range
     */
    public function range(string $name, string $value): Input\Range
    {
        return (new Input\Range($name))->setValue($value);
    }

    /**
     * Generates a section object
     *
     * @param string $content
     * @return Container
     */
    public function section(string $content = ''): Container
    {
        return (new Container('section'))->setContent($content);
    }

    /**
     * Sets the html type to html 5
     * @deprecated
     */
    public function setHTML5(): void
    {
        trigger_error('Html generator only supports HTML5', E_DEPRECATED);
    }

    /**
     * Sets the HTML type for the helper
     *
     * @param string $type The HTML type (html|xhtml)
     * @deprecated
     */
    public function setHtmlType(string $type): void
    {
        trigger_error('Html generator only supports HTML5', E_DEPRECATED);
    }

    /**
     * Generates a select list
     *
     * @param string $name
     * @return  Form\Select
     */
    public function select(string $name): Form\Select
    {
        return new Form\Select($name);
    }

    /**
     * Generates a span
     *
     * @param string $content
     * @return  Container
     */
    public function span(string $content = ''): Container
    {
        return (new Container('span'))->setContent('');
    }

    /**
     * Generates the stylesheet tags for inpage CSS
     *
     * @param string $css
     * @return Head\Stylesheet
     */
    public function stylesheet(string $css): Head\StyleSheet
    {
        return (new Head\Stylesheet())->setCss($css);
    }

    /**
     * Generates a link to a external stylesheet
     *
     * @param string $link
     * @param string $media
     * @return   Head\StylesheetLink
     */
    public function stylesheetLink(string $link, string $media = 'screen'): Head\StylesheetLink
    {
        return (new Head\StylesheetLink($link))->setMedia($media);
    }

    /**
     * @return  Table\Table
     */
    public function table(): Table\Table
    {
        return new Table\Table();
    }

    /**
     * @return  Table\TableRow
     * @deprecated
     */
    public function tableRow(): Table\TableRow
    {
        trigger_error('tableRow is deprecated. Generate the rows from the table.', E_DEPRECATED);

        return new Table\TableRow();
    }

    /**
     * @param string $value
     * @return  Table\TableCell
     * @deprecated
     */
    public function tableCell(string $value = ''): Table\TableCell
    {
        trigger_error('tableCell is deprecated. Generate the rows from the table row.', E_DEPRECATED);

        return (new Table\TableCell())->setContent($value);
    }

    /**
     * Generates a multiply row text input field
     *
     * @param string $name
     * @param string $value
     * @param string $content
     * @return  Form\Textarea
     */
    public function textarea(string $name, string $value = '', string $content = ''): Form\Textarea
    {
        return (new Form\Textarea($name, $value))->setContent($content);
    }

    /**
     * Generates a list
     *
     * @param bool $numbered True when a numbered list is needed, default false
     * @return  Lists\UnNumberedList|Lists\NumberedList
     * @deprecated
     */
    public function unList(bool $numbered = false)
    {
        trigger_error('unlist is deprecated. Use numberedList or unNumberedList instead.', E_DEPRECATED);
        if ($numbered) {
            return $this->numberedList();
        }
        return $this->unNumberedList();
    }

    /**
     * @return Lists\UnNumberedList
     */
    public function unNumberedList(): Lists\UnNumberedList
    {
        return new Lists\UnNumberedList();
    }

    /**
     * Generates a video object
     *
     * @param string $url
     * @param string $type
     * @return Video
     */
    public function video(string $url, string $type = 'WebM'): Video
    {
        return new Video($url, $type);
    }
}