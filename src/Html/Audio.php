<?php
declare(strict_types=1);

namespace youconix\Core\Html;

use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Audio implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    protected $tag;

    /** @var array */
    protected $sources = [];

    /** @var bool */
    protected $autoplay = false;

    /** @var bool */
    protected $controls = false;

    /** @var bool */
    protected $loop = false;

    /**
     * @var bool
     */
    protected $muted = false;

    /** @var string */
    protected $preload = '';

    /**
     * @param string $url
     * @param string $type
     */
    public function __construct(string $url, string $type)
    {
        $this->addSource($url, $type);

        $this->tag = '<audio {settings}{between}>' . PHP_EOL . '{sources}' . PHP_EOL . '</audio>' . PHP_EOL;
    }

    /**
     * Sets the auto play
     *
     * @param bool $autoplay
     * @return Audio
     */
    public function autoplay(bool $autoplay): Audio
    {
        $this->autoplay = $autoplay;
        return $this;
    }

    /**
     * Shows the controls
     *
     * @param bool $controls
     * @return Audio
     */
    public function controls(bool $controls): Audio
    {
        $this->controls = $controls;
        return $this;
    }

    /**
     * @param bool $muted
     * @return Audio
     */
    public function muted(bool $muted): Audio
    {
        $this->muted = $muted;
        return $this;
    }

    /**
     * Plays endless
     *
     * @param bool $loop
     * @return Audio
     */
    public function loop(bool $loop): Audio
    {
        $this->loop = $loop;
        return $this;
    }

    /**
     * Sets the preload action
     * Note: The preload attribute is ignored if autoplay is set.
     *
     * @param string $action (auto|metadata|none)
     * @return Audio
     */
    public function setLoader(string $action): Audio
    {
        if ($action == 'auto' || $action == 'metadata' || $action == 'none') {
            $this->preload = $action;
        }

        return $this;
    }

    /**
     * Adds a source
     *
     * @param string $url
     * @param string $type
     * @return Audio
     */
    public function addSource(string $url, string $type): Audio
    {
        $this->sources[] = $this->createSourceTag($url, $type);
        return $this;
    }

    /**
     * @param string $url
     * @param string $type
     * @return string
     */
    protected function createSourceTag(string $url, string $type): string
    {
        return '<source src="' . $url . '" type="audio/' . $type . '">';
    }

    /**
     * @return string
     */
    protected function createSettings(): string
    {
        if ($this->autoplay) {
            $this->preload = '';
        }

        $settings = '';
        if ($this->autoplay) {
            $settings .= 'autoplay="autoplay" ';
        }
        if ($this->controls) {
            $settings .= 'controls="controls" ';
        }
        if ($this->loop) {
            $settings .= 'loop="loop" ';
        }
        if ($this->muted) {
            $settings .= 'muted="muted" ';
        }
        if (!empty($this->preload)) {
            $settings .= 'preload="' . $this->preload . '" ';
        }

        return $settings;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $settings = $this->createSettings();

        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{settings}', '{sources}'],
            [$between, $settings, implode(PHP_EOL, $this->sources)],
            $this->tag
        );
    }
}