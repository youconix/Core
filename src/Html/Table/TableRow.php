<?php
declare(strict_types=1);

namespace youconix\Core\Html\Table;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class TableRow implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    private $tag;

    /** @var TableCell[] */
    private $cells = [];

    public function __construct()
    {
        $this->tag = '<tr {between}>' . PHP_EOL . '{cells}' . PHP_EOL . '</tr>';
    }

    /**
     * @return TableCell
     */
    public function createCell(): TableCell
    {
        return new TableCell();
    }

    /**
     * @param TableCell $cell
     * @return TableRow
     */
    public function addCell(TableCell $cell): TableRow
    {
        $this->cells[] = $cell;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        $cells = [];
        foreach ($this->cells as $cell) {
            $cells[] = $cell->generateItem();
        }

        return str_replace(
            ['{between}', '{cells}'],
            [$between, implode(PHP_EOL, $cells)],
            $this->tag
        );
    }
}