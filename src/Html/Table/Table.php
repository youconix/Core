<?php
declare(strict_types=1);

namespace youconix\Core\Html\Table;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Table implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    private $tag;

    /** @var TableRow[] */
    private $header = [];

    /** @var TableRow[] */
    private $rows = [];

    /** @var TableRow[] */
    private $footer = [];

    public function __construct()
    {
        $this->tag = '<table {between}>' . PHP_EOL . '{rows}</table>' . PHP_EOL;
    }

    /**
     * @return TableRow
     */
    public function createRow(): TableRow
    {
        return new TableRow();
    }

    /**
     * @return TableCell
     */
    public function createCell(): TableCell
    {
        return new TableCell();
    }

    /**
     * @param TableRow $tableRow
     * @return Table
     */
    public function addHeaderRow(TableRow $tableRow): Table
    {
        $this->header[] = $tableRow;
        return $this;
    }

    /**
     * @param TableRow $tableRow
     * @return Table
     */
    public function addRow(TableRow $tableRow): Table
    {
        $this->rows[] = $tableRow;
        return $this;
    }

    /**
     * @param TableRow $tableRow
     * @return Table
     */
    public function addFooterRow(TableRow $tableRow): Table
    {
        $this->footer[] = $tableRow;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        $rows = $this->generateTableHeader() .
            $this->generateTableBody() .
            $this->generateTableFooter();

        return str_replace(
            ['{between}', '{rows}'],
            [$between, $rows],
            $this->tag
        );
    }

    /**
     * @return string
     */
    private function generateTableHeader(): string
    {
        if (count($this->header) === 0) {
            return '';
        }

        $rows = [];
        foreach ($this->header as $row) {
            $rows[] = $row->generateItem();
        }

        return '<thead>' . PHP_EOL . implode(PHP_EOL, $rows) . PHP_EOL . '</thead>' . PHP_EOL;
    }

    /**
     * @return string
     */
    private function generateTableBody(): string
    {
        $rows = [];
        foreach ($this->rows as $row) {
            $rows[] = $row->generateItem();
        }

        return '<tbody>' . PHP_EOL . implode(PHP_EOL, $rows) . PHP_EOL . '</tbody>' . PHP_EOL;
    }

    /**
     * @return string
     */
    private function generateTableFooter(): string
    {
        if (count($this->footer) === 0) {
            return '';
        }

        $rows = [];
        foreach ($this->footer as $row) {
            $rows[] = $row->generateItem();
        }

        return '<tfoot>' . PHP_EOL . implode(PHP_EOL, $rows) . PHP_EOL . '</tfoot>' . PHP_EOL;
    }
}