<?php
declare(strict_types=1);

namespace youconix\Core\Html\Table;

use youconix\Core\Html\Container;

class TableCell extends Container
{
    /** @var string */
    private $rowspan = '';

    /** @var string */
    private $colspan = '';

    public function __construct()
    {
        $this->tag = '<td {colspan}{rowspan}{between}>{content}</td>';
    }

    /**
     * @param int $rowSpan
     * @return TableCell
     */
    public function setRowSpan(int $rowSpan): TableCell
    {
        $this->rowspan = 'rowspan="' . $rowSpan . '" ';
        return $this;
    }

    /**
     * @param int $colspan
     * @return TableCell
     */
    public function setColSpan(int $colspan): TableCell
    {
        $this->colspan = 'colspan="' . $colspan . '" ';
        return $this;
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $result = parent::generateItem();

        return str_replace(
            ['{rowspan}', '{colspan}'],
            [$this->rowspan, $this->colspan],
            $result
        );
    }
}