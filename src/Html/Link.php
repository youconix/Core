<?php
declare(strict_types=1);

namespace youconix\Core\Html;

use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;
use youconix\Core\Html\Traits\RelationTrait;

class Link implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;
    use RelationTrait;

    /** @var string */
    private $tag;

    /** @var string */
    private $url = '';

    /** @var string */
    private $value;

    /**
     * @param string $url
     * @param string $value
     */
    public function __construct(string $url, string $value = '')
    {
        $this->url = $url;
        $this->value = !empty($value) ? $value : $url;

        $this->tag = '<a href="{url}" {between}>{value}</a>';
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss() .
            $this->relation;

        return str_replace(
            ['{url}', '{between}', '{value}'],
            [$this->url, $between, $this->value],
            $this->tag
        );
    }
}