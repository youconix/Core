<?php
declare(strict_types=1);

namespace youconix\Core\Html;

class Video extends Audio
{
    /** @var string */
    private $poster = '';

    /**
     * @param string $url
     * @param string $type
     */
    public function __construct(string $url, string $type)
    {
        $this->addSource($url, $type);

        $this->tag = '<video {settings}{between}>' . PHP_EOL . '{sources}' . PHP_EOL . '</video>' . PHP_EOL;
    }

    /**
     * Sets the loading icon
     *
     * @param string $icon
     * @return Video
     */
    public function setPreLoader(string $icon): Video
    {
        $this->poster = $icon;

        return $this;
    }

    /**
     * @param string $url
     * @param string $type
     * @return string
     */
    protected function createSourceTag(string $url, string $type): string
    {
        return '<source src="' . $url . '" type="video/' . $type . '">';
    }

    /**
     * @return string
     */
    protected function createSettings(): string
    {
        $settings = parent::createSettings();

        if (!empty($this->poster)) {
            $settings .= 'poster="' . $this->poster . '" ';
        }

        return $settings;
    }
}