<?php
declare(strict_types=1);

namespace youconix\Core\Html\Traits;

trait CssTrait
{
    /** @var array */
    protected $classes = [];

    /** @var string */
    protected $style = '';

    /**
     * Adds a CSS class
     * @param string $class
     * @return $this
     */
    public function addClass(string $class): self
    {
        $this->classes[] = $class;
        return $this;
    }

    /**
     * @param string $style
     * @return $this
     */
    public function setStyle(string $style): self
    {
        $this->style = $style;
        return $this;
    }

    /**
     * @return string
     */
    protected function parseCss(): string
    {
        $css = '';
        if (!empty($this->style)) {
            $css = 'style="' . $this->style . '" ';
        }

        if (count($this->classes) > 0) {
            $css .= 'class="' . implode(' ', $this->classes) . '" ';

        }
        return $css;
    }
}