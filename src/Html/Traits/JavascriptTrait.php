<?php
declare(strict_types=1);

namespace youconix\Core\Html\Traits;

trait JavascriptTrait
{
    /** @var string */
    protected $id = '';

    /** @var array */
    protected $data = [];

    /** @var array */
    protected $events = [];

    /**
     * Overwrites the id if a id is already active
     *
     * @param string $id
     * @return $this
     */
    public function setID($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Sets a data item
     *
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function setData(string $name, string $value): self
    {
        $this->data[] = [$name, $value];
        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function setEvent($name, $value): self
    {
        $this->events[] = [$name, $value];
        return $this;
    }

    /**
     * @return string
     */
    protected function parseJavaScript(): string
    {
        $javaScript = '';
        if (!empty($this->id)) {
            $javaScript .= 'id="' . $this->id . '" ';
        }
        foreach ($this->data as $item) {
            $javaScript .= 'data-' . $item[0] . ' = "' . $item[1] . '" ';
        }
        foreach ($this->events as $event) {
            $javaScript .= 'on' . ucfirst($event[0]) . ' = "' . $event[1] . '" ';
        }

        return $javaScript;
    }
}