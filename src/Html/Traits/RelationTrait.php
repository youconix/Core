<?php
declare(strict_types=1);

namespace youconix\Core\Html\Traits;

trait RelationTrait
{
    /** @var string */
    protected $relation = '';

    /**
     * Sets the rel-attribute
     *
     * @param string $relation
     * @return $this
     */
    public function setRelation(string $relation): self
    {
        $this->relation = 'rel="' . $relation . '" ';
        return $this;
    }
}