<?php
declare(strict_types=1);

namespace youconix\Core\Html;

interface HtmlItemInterface
{
    /**
     * @return string
     */
    public function __toString(): string;

    /**
     * @return string
     */
    public function generateItem(): string;
}