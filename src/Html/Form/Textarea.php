<?php
declare(strict_types=1);

namespace youconix\Core\Html\Form;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Textarea implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    private $tag;

    /** @var string */
    private $content;

    /** @var string */
    private $rows = '';

    /** @var string */
    private $cols = '';

    /**
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, string $value)
    {
        $this->setContent($value);

        $this->tag = '<textarea {rows}{cols} name="'.$name.'" {between}>{content}</textarea>';
    }

    /**
     * @param string|HtmlItemInterface $content
     * @return $this
     */
    public function setContent($content): Textarea
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function addContent(string $content): Textarea
    {
        $this->content .= $content;
        return $this;
    }

    /**
     * @param int $rows
     * @return $this
     */
    public function setRows(int $rows): Textarea
    {
        $this->rows = 'rows="'.$rows.'" ';
        return $this;
    }

    /**
     * @param int $cols
     * @return $this
     */
    public function setCols(int $cols): Textarea
    {
        $this->cols = 'cols="'.$cols.'" ';
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{content}', '{rows}', '{cols}'],
            [$between, $this->content , $this->rows, $this->cols],
            $this->tag
        );
    }
}