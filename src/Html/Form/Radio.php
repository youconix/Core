<?php
declare(strict_types=1);

namespace youconix\Core\Html\Form;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Radio implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    protected $tag;

    /** @var bool */
    protected $checked = false;

    /** @var String */
    protected $value;

    /**
     * @param String $name The name
     */
    public function __construct(string $name)
    {
        $this->tag = '<input type="radio" name="' . $name . '" {value}{checked}{between}>';
    }

    /**
     * @param string $value
     * @return Radio
     */
    public function setValue(string $value): Radio
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Radio
     */
    public function setChecked(): Radio
    {
        $this->checked = true;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $checked = $this->checked ? 'checked="checked" ' : '';
        $value = !empty($this->value) ? 'value="' . $this->value . '" ' : '';

        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{checked}', '{value}'],
            [$between, $checked, $value],
            $this->tag
        );
    }
}