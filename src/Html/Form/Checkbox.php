<?php
declare(strict_types=1);

namespace youconix\Core\Html\Form;

class Checkbox extends Radio
{
    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->tag = '<input type="checkbox" name="' . $name . '" {value}{checked}{between}>';
    }
}