<?php
declare(strict_types=1);

namespace youconix\Core\Html\Form;

use youconix\Core\Html\Container;

class Form extends Container
{
    /**
     * @param string $link
     * @param string $method
     * @param bool $isMultiData
     */
    public function __construct(string $link, string $method, bool $isMultiData = false)
    {
        $multiData = $isMultiData ? 'enctype="multipart/form-data" ' : '';

        $this->tag = '<form action="' . $link . '" method="' . $method . '" ' . $multiData . '{between}>' . PHP_EOL . '{content}' . PHP_EOL . '</form>';
    }
}