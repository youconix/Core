<?php
declare(strict_types=1);

namespace youconix\Core\Html;

use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Container implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    protected $tag;

    /** @var string */
    protected $content;

    /**
     * @param string $tag
     */
    public function __construct(string $tag)
    {
        $this->tag = '<' . $tag . ' {between}>{content}</' . $tag . '>';
    }

    /**
     * @param string|HtmlItemInterface $content
     * @return Container
     * @throws \InvalidArgumentException
     */
    public function setContent($content): Container
    {
        $this->content = $this->parseContent($content);
        return $this;
    }

    /**
     * @param string|HtmlItemInterface $content
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function parseContent($content): string
    {
        if (is_object($content)) {
            if (!$content instanceof HtmlItemInterface) {
                throw new \InvalidArgumentException('Only Html items can be automatically parsed.');
            }

            return $content->generateItem();
        }

        return $content;
    }

    /**
     * @param string $content
     * @return Container
     */
    public function addContent(string $content): Container
    {
        $this->content .= $content;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{content}'],
            [$between, $this->content],
            $this->tag
        );
    }
}