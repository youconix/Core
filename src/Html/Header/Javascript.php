<?php
declare(strict_types=1);

namespace youconix\Core\Html\Header;

use youconix\Core\Html\HtmlItemInterface;

class Javascript implements HtmlItemInterface
{
	/** @var string */
	private $tag;

	/** @var string */
	private $type;

	/** @var string */
	private $javascript;

	public function __construct()
	{
		$this->tag = '<script {type}>' . PHP_EOL . '<!--' . PHP_EOL . '{javascript}' . PHP_EOL . '//-->' . PHP_EOL . '</script>';
	}

	/**
	 * @param string $type
	 * @return Javascript
	 */
	public function setType(string $type): Javascript
	{
		$this->type = 'type="' . $type . '"';
		return $this;
	}

	/**
	 * @param string $javascript
	 * @return Javascript
	 */
	public function setJavascript(string $javascript): Javascript
	{
		$this->javascript = $javascript;
		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->generateItem();
	}

	/**
	 * @return string
	 */
	public function generateItem(): string
	{
		return str_replace(['{type}', '{javascript}'],
			[$this->type, $this->javascript],
			$this->tag
		);
	}
}