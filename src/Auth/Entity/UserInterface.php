<?php

namespace youconix\Core\Auth;

use DateTime;
use Doctrine\Common\Collections\Collection;
use stdClass;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;
use youconix\Core\Entity\Role;

interface UserInterface extends BaseUserInterface
{
    /**
     * @return UserInterface
     */
    public function disableAccount(): UserInterface;

    /**
     * @return UserInterface
     */
    public function enableAccount(): UserInterface;

    /**
     * @return bool
     */
    public function isBindToIp(): bool;

    /**
     * @return bool
     */
    public function isBlocked(): bool;

    /**
     * @return bool
     */
    public function isBot(): bool;

    /**
     * @return string
     */
    public function getActivationCode(): string;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * Alias of getUserid
     * @return int
     */
    public function getID();

    /**
     * @param int $groupId
     * @return null|Role[]
     * @deprecated
     */
    public function getGroup(int $groupId): ?Role;

    /**
     * Alias of getRoles
     * @return Role[]|Collection
     * @deprecated
     */
    public function getGroups(): Collection;

    /**
     * @return string
     */
    public function getLanguage(): string;

    /**
     * @return DateTime|null
     */
    public function getLastLogin(): ?DateTime;

    /**
     * @return DateTime
     */
    public function getRegistrated(): DateTime;

    /**
     * @return int
     */
    public function getUserid();

    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @param string $activationCode
     * @return UserInterface
     */
    public function setActivationCode(string $activationCode): UserInterface;

    /**
     * @param bool $bindToIp
     * @return UserInterface
     */
    public function setBindToIp(bool $bindToIp): UserInterface;

    /**
     * @param bool $isBlocked
     * @return UserInterface
     */
    public function setBlocked(bool $isBlocked): UserInterface;

    /**
     * @param bool $isBbot
     * @return UserInterface
     */
    public function setBot(bool $isBot): UserInterface;

    /**
     * Alias of setRoles
     * @param Role[]|Collection $groups
     * @return UserInterface
     * @deprecated
     */
    public function setGroups(Collection $groups): UserInterface;

    /**
     * @param string $email
     * @return UserInterface
     */
    public function setEmail(string $email): UserInterface;

    /**
     * @param bool $enabled
     * @return UserInterface
     */
    public function setEnabled(bool $enabled): UserInterface;

    /**
     * @param string $language
     * @return UserInterface
     */
    public function setLanguage(string $language): UserInterface;

    /**
     * @param DateTime $loggedIn
     * @return UserInterface
     */
    public function setLastLogin(DateTime $loggedIn): UserInterface;

    /**
     * @param string $passwordHash
     * @return UserInterface
     */
    public function setPassword(string $passwordHash): UserInterface;

    /**
     * @param DateTime $registrated
     * @return UserInterface
     */
    public function setRegistrated(DateTime $registrated): UserInterface;

    /**
     * @param Role[]|Collection $roles
     * @return UserInterface
     */
    public function setRoles(Collection $roles): UserInterface;

    /**
     * @param string $username
     * @return UserInterface
     */
    public function setUsername(string $username): UserInterface;
}