<?php

namespace youconix\Core\Auth\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use youconix\Core\Entity\Role;

class Login implements UserInterface
{
    /** @var Role[]|Collection */
    protected $roles;

    /** @var int */
    protected $userid = null;

    /** @var string */
    protected $username = '';

    /** @var string */
    protected $email = '';

    /** @var bool */
    protected $bot = false;

    /** @var DateTime */
    protected $registrated;

    /** @var DateTime */
    protected $lastLogin;

    /** @var bool */
    protected $enabled = false;

    /** @var bool */
    protected $blocked = false;

    /** @var bool */
    protected $passwordExpired = false;

    /** @var string */
    protected $salt;

    /** @var string */
    protected $password;

    /** @var string */
    protected $activationCode;

    /** @var string */
    protected $language;

    /** @var bool */
    protected $bindToIp = false;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function disableAccount(): UserInterface
    {
        $this->enabled = false;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function enableAccount(): UserInterface
    {
        $this->enabled = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials(): UserInterface
    {
        $this->password = null;
        $this->salt = null;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isBindToIp(): bool
    {
        return $this->bindToIp;
    }

    /**
     * @inheritDoc
     */
    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    /**
     * @inheritDoc
     */
    public function isBot(): bool
    {
        return $this->bot;
    }

    /**
     * @inheritDoc
     */
    public function getActivationCode(): string
    {
        return $this->activationCode;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getID()
    {
        return $this->userid;
    }

    /**
     * @inheritDoc
     */
    public function getGroup(int $groupId): ?Role
    {
        foreach ($this->roles as $role)
        {
            if ($role->getId() === $groupId)
            {
                return $role;
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getGroups(): Collection
    {
        return $this->roles;
    }

    /**
     * @inheritDoc
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @inheritDoc
     */
    public function getLastLogin(): ?DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRegistrated(): DateTime
    {
        return $this->registrated;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @inheritDoc
     */
    public function setActivationCode(string $activationCode): UserInterface
    {
        $this->activationCode = $activationCode;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setBindToIp(bool $bindToIp): UserInterface
    {
        $this->bindToIp = $bindToIp;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setBlocked(bool $isBlocked): UserInterface
    {
        $this->blocked = $isBlocked;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setBot(bool $isBot): UserInterface
    {
        $this->bot = $isBot;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setGroups(Collection $groups): UserInterface
    {
        $this->roles = $groups;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setEmail(string $email): UserInterface
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setEnabled(bool $enabled): UserInterface
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setLanguage(string $language): UserInterface
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setLastLogin(DateTime $loggedIn): UserInterface
    {
        $this->lastLogin = $loggedIn;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPassword(string $passwordHash): UserInterface
    {
        $this->password = $passwordHash;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setRegistrated(DateTime $registrated): UserInterface
    {
        $this->registrated = $registrated;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setRoles(Collection $roles): UserInterface
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setUsername(string $username): UserInterface
    {
        $this->username = $username;
        return $this;
    }
}