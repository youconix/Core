<?php

namespace core\database;

/**
 * General database configuration layer                                         
 * Loads the preset DAL                                                         
 *                                                                              
 * This file is part of Scripthulp framework                                    
 *                                                                              
 * @copyright 2012,2013,2014  Rachelle Scheijen                                
 * @author    Rachelle Scheijen                                                
 * @since     1.0                                                              
 * @changed   09/09/12                                                          
 *                                                                              
 * Scripthulp framework is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Lesser General Public License as published by  
 * the Free Software Foundation, either version 3 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * Scripthulp framework is distributed in the hope that it will be useful,      
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *                                                                              
 * You should have received a copy of the GNU Lesser General Public License     
 * along with Scripthulp framework.  If not, see <http://www.gnu.org/licenses/>.
 */
class Query_main {

    private $service_Settings;
    private $s_type;

    /**
     * Includes the correct DAL
     * 
     * \core\services\Settings $service_Settings  The settings service
     */
    public function __construct(\core\services\Settings $service_Settings) {
        /* databasetype */
        $this->s_type = ucfirst($service_Settings->get('settings/SQL/type'));
        $this->service_Settings = $service_Settings;
    }

    /**
     * Loads the selected DAL
     *
     * @return		Object      The selected DAL-object
     */
    public function loadDatabase() {
        require_once(NIV . 'include/database/' . $this->s_type . '.inc.php');

        $s_name = '\core\database\Database_' . $this->s_type;
        $obj_DAL = new $s_name($this->service_Settings);
        $obj_DAL->defaultConnect();

        return $obj_DAL;
    }

}

interface DAL {


}

?>
