<?php

namespace core\services;

use youconix\Core\Database\DAL;

/**
 * General query builder configuration layer
 * Loads the preset query builder
 *
 * This file is part of Scripthulp framework
 *
 * @copyright 		2014,2015,2016  Rachelle Scheijen
 * @author        Rachelle Scheijen
 * @since         1.0
 * @changed   		30/03/2014
 *
 * Scripthulp framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scripthulp framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Scripthulp framework.  If not, see <http://www.gnu.org/licenses/>.
 */

class QueryBuilder extends Service{
  private $service_Database;
	private $obj_builder;
	private $s_type;

	/**
	 * Includes the correct DAL
   * 
   * @param core\services\Settings $service_Settings    The settings service
   * @param DAL      $service_Database    The DAL
	 */
	public function __construct(\core\services\Settings $service_Settings, DAL $service_Database){
    $this->service_Database = $service_Database;

		/* databasetype */
		$this->s_type           = ucfirst($service_Settings->get('settings/SQL/type'));
		$this->loadBuilder();
	}

	/**
	 * Loads the selected Builder
	 */
	private function loadBuilder(){
		require_once(NIV.'include/database/builder_'.$this->s_type.'.inc.php');

		$this->service_Database->defaultConnect();
	}
	
  /**
   * Creates the builder
   * 
   * @return Builder    The builder
   */
	public function createBuilder(){
		if( is_null($this->obj_builder) ){
			$s_name     = '\core\database\Builder_'.$this->s_type;
			$this->obj_builder    = new $s_name($this->service_Database);
		}
		
		return $this->obj_builder;
	}
}
